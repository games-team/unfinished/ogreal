Source: ogreal
Priority: optional
Section: libs
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Federico Di Gregorio <fog@debian.org>
Build-Depends: debhelper (>= 5), tofrodos, quilt, libtool, automake, pkg-config, chrpath, libogre-dev (>= 1.4), libvorbis-dev, libogg-dev, libalut-dev, libopenal-dev, libois-dev
Standards-Version: 3.7.3
Homepage: http://sourceforge.net/projects/ogreal/
Vcs-Git: git://git.debian.org/git/pkg-games/ogreal.git
Vcs-Browser: http://git.debian.org/?p=pkg-games/ogreal.git
DM-Upload-Allowed: yes

Package: libogreal0
Section: libs
Architecture: any
Depends: ${shlibs:Depends}
Description: OpenAL plugin for the Ogre Rendering Engine (libraries)
 OgreAL is an OpenAL wrapper for the Ogre Rendering Engine. It allows you to
 add 3D sounds and music to your games and applications in a clean and easy
 way.
 .
 This package contains the OgreAL library.

Package: libogreal0-dbg
Section: libdevel
Architecture: any
Priority: extra
Depends: libogreal0 (= ${binary:Version})
Description: OpenAL plugin for the Ogre Rendering Engine (debugging libs)
 OgreAL is an OpenAL wrapper for the Ogre Rendering Engine. It allows you to
 add 3D sounds and music to your games and applications in a clean and easy
 way.
 .
 This package contains the debugging version of the OgreAL library.

Package: libogreal-dev
Section: libdevel
Architecture: any
Depends: libogreal0 (= ${binary:Version}), libogre-dev (>= 1.4), libvorbis-dev, libogg-dev, libalut-dev, libopenal-dev
Description: OpenAL plugin for the Ogre Rendering Engine (development files)
 OgreAL is an OpenAL wrapper for the Ogre Rendering Engine. It allows you to
 add 3D sounds and music to your games and applications in a clean and easy
 way.
 .
 This package contains the headers and static libraries needed to develop
 with OgreAL. This package also contains the OgreAL demos along with its source
 code to learn from.
